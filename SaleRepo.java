package com.example.Project;

import com.example.Project.model.Book;
import com.example.Project.model.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SaleRepo extends JpaRepository<Sale,String> {

    List<Sale> findByDate(int date);
}
