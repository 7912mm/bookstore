package com.example.Project.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Sale {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String book;
    private String user;
    private int date;
    private int price;


    private String role;
    private Boolean enabled;
    public Sale() {}

    @OneToMany
    private List<Book> bookList;

    public Sale(String book, String customer, int date, int value) {
        this.book = book;
        this.user = user;
        this.date = date;
        this.price = price;


    }

    public void setSaleid(long id) {
        this.id = id;
    }

    public long getSaleid() {
        return id;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getBook() {
        return book;
    }

    public String getUser() {
        return user;
    }

    public int getDate() {
        return date;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Sale{" + "book=" + book + ", user=" + user + ", date=" + date + ", price=" + price + "};
    }
}
