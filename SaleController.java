package com.example.Project;

import com.example.Project.model.Book;
import com.example.Project.model.Sale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RestController
public class SaleController {

    @Autowired
    SaleRepo saleRepo;


    @GetMapping("/")
    public List<Sale> listAllSales(){
        List<Sale> sales = new ArrayList<>();
        saleRepo.findAll().forEach(sales::add);

        if(sales.size()==0){
            throw new NullPointerException();
        }

        return sales;
    }

    @PostMapping("/")
    public Sale createSale(@RequestBody Sale sale){
        return saleRepo.save(sale);
    }

    @PutMapping("/{id}")
    public Sale createSale(@PathVariable Long id, @RequestBody Sale sale){
        Optional<Sale> foundSale= saleRepo.findById(id);
        if(foundSale.isEmpty()){
            throw  new NullPointerException();
        }
        Sale saleToEdit = foundSale.get();
        saleToEdit.setDate(sale.getDate());
        saleToEdit.setPrice(sale.getPrice());

        return saleRepo.save(saleToEdit);
    }

    @GetMapping("/search")
    public List<Sale> findbyId(@RequestParam( name="id", required = true) Long id){
        return saleRepo.findById(id);
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "sadsdasd")
    @ExceptionHandler(NullPointerException.class)
    public void noBooksFound(){
    }

}

