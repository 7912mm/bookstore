package com.example.Project.model;

import javax.persistence.*;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long bookid;

    private String name;
    private String author;
    private double value;
    private int quantity;

    private String role;
    private Boolean enabled;
    public Book() {}

    @ManyToOne
    private Sale sale;

    public Book(String name, String author, double value, int quantity) {
        this.name = name;
        this.author = author;
        this.value = value;
        this.quantity = quantity;
    }

    public void setBookid(long bookid) {
        this.bookid = bookid;
    }

    public long getBookid() {
        return bookid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public double getValue() {
        return value;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public String toString() {
        return "Book{" + ", name=" + name + ", author=" + author + ", value=" + value + "};
    }
}
