package com.example.Project;

import com.example.Project.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookController {

    @Autowired
    List<Book> library;


    @Value("${test}")
    private String var;

    @Value("${email}")
    private String email;




    @GetMapping("/hello")
    public String hello(){
        return "Hello This is the bookstore. You can contact us on " + email;
    }

    @GetMapping("/book")
    public List<Book> books(@RequestParam(name = "book", required = false) String bookName){
        if(bookName!=null){
            List<Book> booksToReturn=new ArrayList<>();
            for(Book book:library){
                if(book.getName().equals(bookName)){
                    booksToReturn.add(book);
                }
            }
            return booksToReturn;
        } else {
            return library;
        }
    }

    @PostMapping("/book")
    public Book createBook(@RequestBody Book book){
        System.out.println(book);
        library.add(book);
        return book;
    }


    //@PutMapping Update Object
    @PutMapping("/book")
    public Book updateBook(@RequestBody Book book){
        String name = book.getName();
        for(Book updateBook: library){
            if(name.equals(updateBook.getName())){
                updateBook.setValue(book.getValue());
                return updateBook;
            }
        }
        return null;
    }

    //@DeleteMapping Delete Object
    @DeleteMapping("/book")
    public Book deleteBook(@RequestParam (name = "book", required = true) String BookName){
        for(Book deleteBook:library){
            if(BookName.equals(deleteBook.getName())){
                library.remove(deleteBook);
                return deleteBook;
            }
        }
        return null;
    }
}

