package com.example.Project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
    public class User{

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private long id;
        //@NotBlank(message = "Name is mandatory")
        private String name;

        //@NotBlank(message = "Email is mandatory")
        private String email;

        private String address;


        private String role;
        private Boolean enabled;
        public User() {}


    public User(String name, String email, String address) {
            this.name = name;
            this.email = email;
            this.address = address;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getId() {
            return id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setAddress(String address) { this.address = address; }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getAddress() { return address; }



        @Override
        public String toString() {
            return "User{" + "id=" + id + ", name=" + name + ", address=" + address + ", email=" + email + '}';
        }
    }

